<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->createRole('User');
        $admin = $auth->createRole('Admin');

        $auth->add($user);
        $auth->add($admin);
    }

     public function actionAddAdmin()
     {
         $user = User::findOne(51);

         $auth = Yii::$app->authManager;
         $admin = $auth->getRole('Admin');
         $auth->assign($admin, $user->id);
     }
}