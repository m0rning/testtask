<?php

namespace app\controllers;

use app\models\Transactions;
use app\models\UserCredits;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

class TransactionsController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionSendCredits()
    {
        $tansaction = new Transactions();

        if ($tansaction->load(Yii::$app->request->post())) {
            $tansaction->type = $tansaction::TYPE_TRANSACTION;

            if ($tansaction->save()) {
                return $this->redirect(['site/index']);
            }
        }
        return $this->render('send-credits',['model' => $tansaction]);
    }

    public function actionSendBill()
    {
        $tansaction = new Transactions();

        if ($tansaction->load(Yii::$app->request->post())) {
            $tansaction->type = $tansaction::TYPE_BILL;

            if ($tansaction->save()) {
                return $this->redirect(['site/index']);
            }
        }
        return $this->render('send-bill',['model' => $tansaction]);
    }

    public function actionUserHistory()
    {

        $pendingBills = new ActiveDataProvider([
            'query' => Transactions::find()->where(['reciever_ip' => Yii::$app->user->identity->user_ip, 'type' => '1', 'status' => 10]),
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        $doneBills = new ActiveDataProvider([
            'query' => Transactions::find()->where(['reciever_ip' => Yii::$app->user->identity->user_ip, 'type' => '1'])->andWhere('status < 10'),
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        $pendingTransactions = new ActiveDataProvider([
            'query' => Transactions::find()->where(['reciever_ip' => Yii::$app->user->identity->user_ip, 'type' => '0', 'status' => 10]),
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        $doneTransactions = new ActiveDataProvider([
            'query' => Transactions::find()->where(['reciever_ip' => Yii::$app->user->identity->user_ip, 'type' => '0'])->andWhere('status < 10'),
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->render('user-notifications',[
            'pendingBills' => $pendingBills,
            'doneBills' => $doneBills,
            'pendingTransactions' => $pendingTransactions,
            'doneTransactions' => $doneTransactions
        ]);
    }

    public function actionAcceptBill($id)
    {
        $bill = Transactions::find()->where(['id' => $id])->one();
        $this->manipulateCredits($bill);
        return $this->redirect('user-history');
    }

    public function actionDeclineBill($id)
    {
        $bill = Transactions::find()->where(['id' => $id])->one();
        $bill->status = Transactions::STATUS_DECLINED;

        if ($bill->save()) {
            return $this->redirect('user-history');
        }
    }

    public function actionAcceptTransaction()
    {
        $post = Yii::$app->request->post();
        $transaction = Transactions::find()->where(['id' => $post['modelId']])->one();
        if ($transaction->protection_code == $post['protectionCode']) {
            $this->manipulateCredits($transaction);
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app','Invalid code'));
        }

        return $this->redirect('user-history');
    }

    public function actionDeclineTransaction($id)
    {
        $transaction = Transactions::find()->where(['id' => $id])->one();
        $transaction->status = Transactions::STATUS_DECLINED;

        if ($transaction->save()) {
            return $this->redirect('user-history');
        }
    }

    public function actionReturnTransaction($id)
    {
        $transaction = Transactions::find()->where(['id' => $id])->one();
        $this->manipulateCredits($transaction);
        return $this->redirect('user-history');

    }

    public function actionCreditsHistory() {

        return $this->render('credits-history');
    }

    public function actionBillsHistory() {

        return $this->render('bills-history');
    }

    public function manipulateCredits($transaction) {

        if (($transaction->type == $transaction::TYPE_BILL) && ($transaction->status == $transaction::STATUS_PENDING)) {
            $recieverCredits = UserCredits::find()->where(['user_ip' => $transaction->reciever_ip])->one();
            $recieverCredits->credits_count = $recieverCredits->credits_count - $transaction->credits;

            if ($recieverCredits->save()) {
                $senderCredits = UserCredits::find()->where(['user_ip' => $transaction->sender_ip])->one();
                $senderCredits->credits_count = $senderCredits->credits_count + $transaction->credits;

                if ($senderCredits->save()) {
                    $transaction->status = Transactions::STATUS_DONE;
                    $transaction->save();
                }

            }
        } elseif (($transaction->type == $transaction::TYPE_TRANSACTION) && ($transaction->status == $transaction::STATUS_DONE)) {
            $recieverCredits = UserCredits::find()->where(['user_ip' => $transaction->reciever_ip])->one();
            $recieverCredits->credits_count = $recieverCredits->credits_count - $transaction->credits;

            if ($recieverCredits->save()) {
                $senderCredits = UserCredits::find()->where(['user_ip' => $transaction->sender_ip])->one();
                $senderCredits->credits_count = $senderCredits->credits_count + $transaction->credits;

                if ($senderCredits->save()) {
                    $transaction->status = Transactions::STATUS_DECLINED;
                    $transaction->save();
                }

            }
        }

        elseif (($transaction->type == $transaction::TYPE_TRANSACTION) && ($transaction->status == $transaction::STATUS_PENDING)) {
                $recieverCredits = UserCredits::find()->where(['user_ip' => $transaction->reciever_ip])->one();
                $recieverCredits->credits_count = $recieverCredits->credits_count + $transaction->credits;

                if ($recieverCredits->save()) {
                    $senderCredits = UserCredits::find()->where(['user_ip' => $transaction->sender_ip])->one();
                    $senderCredits->credits_count = $senderCredits->credits_count - $transaction->credits;

                    if ($senderCredits->save()) {
                        $transaction->status = Transactions::STATUS_DONE;
                        $transaction->save();
                    }
                }
        }
    }

}