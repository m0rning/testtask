<?php

namespace app\controllers;

use app\models\UserCredits;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


class UserCreditsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionShowCreditsCount()
    {
        $credits = UserCredits::find()->where(['user_ip' => Yii::$app->user->identity->user_ip])->one();
         return $credits->credits_count;
    }

    public function actionUserNotifications()
    {
        return $this->render('user-notifications');
    }

}