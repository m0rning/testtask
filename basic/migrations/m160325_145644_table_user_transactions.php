<?php

use yii\db\Migration;

class m160325_145644_table_user_transactions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_transactions}}', [
            'id' => $this->primaryKey(),
            'sender_ip' => $this->string()->notNull(),
            'reciever_ip' => $this->string()->notNull(),
            'credits' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'expire' => $this->integer()->notNull(),
            'protection_code' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_transactions}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
