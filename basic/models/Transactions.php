<?php
namespace app\models;

use Yii;
use yii\base\ErrorException;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;


class Transactions extends ActiveRecord
{
    const STATUS_DONE = 0;
    const STATUS_DECLINED = 1;
    const STATUS_PENDING = 10;
    const TYPE_TRANSACTION = 0;
    const TYPE_BILL = 1;
    public $protected;
    public $authKey;


    /**
     * @var array EAuth attributes
     */
    public static function tableName()
    {
        return '{{%user_transactions}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['credits'],'required'],
            [['reciever_ip'],'required'],
            [['credits'],'integer'],
            [['protected'],'integer'],
            [['type'],'integer'],
            [['reciever_ip'],'ip'],
            ['reciever_ip','exist' , 'targetClass' => User::className(), 'targetAttribute' =>  'user_ip' ],
            [['reciever_ip','credits'],'required'],
            ['expire', 'default', 'value' => time() + (1 * 24 * 60 * 60) ],
            ['status', 'default', 'value' => self::STATUS_PENDING],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->sender_ip = Yii::$app->user->identity->user_ip;
            if ($this->type == self::TYPE_TRANSACTION && $this->status == self::STATUS_PENDING) {
                if ($this->protected == 1) {
                    $protectionCode = $this->generateProtectionCode();
                    $this->protection_code = $protectionCode;
                } elseif ($this->protected == 0  && $this->status == self::STATUS_PENDING) {
                    $recieverCredits = UserCredits::find()->where(['user_ip' => $this->reciever_ip])->one();
                    $recieverCredits->credits_count += $this->credits;
                    if ($recieverCredits->save()){
                        $senderCredits = UserCredits::find()->where(['user_ip' => $this->sender_ip])->one();
                        $senderCredits->credits_count -= $this->credits;
                        if ($senderCredits->save()) {
                            $this->status = self::STATUS_DONE;
                            $this->expire = 0;
                        }

                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function generateProtectionCode() {
        $code = md5(time());
        return $code;
    }
}