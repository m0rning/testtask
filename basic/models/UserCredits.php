<?php
namespace app\models;

use Yii;
use yii\base\ErrorException;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;


class UserCredits extends ActiveRecord
{

    /**
     * @var array EAuth attributes
     */
    public static function tableName()
    {
        return '{{%user_credits}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_ip'],'required'],
            [['user_ip'],'ip'],
            ['user_ip','exist' , 'targetClass' => User::className(), 'targetAttribute' =>  'user_ip' ],
            [['credits_count'],'integer'],
        ];
    }

    public function create()
    {
        $this->user_ip = Yii::$app->request->userIP;
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

}