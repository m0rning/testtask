<?php

namespace tests\codeception\unit\models;

use app\models\Transactions;
use app\models\User;
use app\models\UserCredits;
use Codeception\Specify;
use yii\codeception\TestCase;

class TransactionsTest extends TestCase
{
    use Specify;

    public function testCreateTransactionWithIncorrectIP()
    {
        $model = new Transactions([
            'credits' => '123000',
            'reciever_ip' => '123000',
        ]);

        $this->specify('user_ip must be correct value', function () use ($model) {
            expect('model should not create transaction', $model->validate())->false();
        });
    }

    public function testTransactionWithIncorrectCreditsFormat()
    {
        $model = new Transactions([
            'credits' => 'abc',
            'reciever_ip' => '123000',
        ]);

        $this->specify('user_ip must be correct value', function () use ($model) {
            expect('model should not create transaction', $model->validate())->false();
        });
    }

    public function testCreateTransactionWithCorrectIP()
    {
        $model = new Transactions([
            'credits' => '123000',
            'reciever_ip' => '127.0.0.1',
        ]);

        $this->specify('user_ip must be correct value', function () use ($model) {
            expect('model should not create transaction', $model->validate())->true();
        });
    }

}
