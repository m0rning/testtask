<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'sender_ip',
        'reciever_ip',
        'credits',
        'status' => [
            'label' => 'Статус',
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'value' => function ($data) {
                if ($data->status == 1) {
                    return "Отклонен";
                } elseif ($data->status == 10) {
                    return "Ожидает подтверждения";
                } else {
                    return "Подтвержден";
                }

            },
        ],
        'type' => [
            'label' => 'Тип',
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'value' => function ($data) {
                if ($data->type == 1) {
                    return "Счет";
                } else {
                    return "Перевод";
                }

            },
        ],
        // 'expire',
        // 'protection_code',
        // 'created_at',
        // 'updated_at',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]);