<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-3">
                <h2>Переводы</h2>
                <p><?= \yii\bootstrap\Html::a('Перевести сумму', ['transactions/send-credits'], ['class' =>'btn btn-default']) ?></p>
                <p><?= \yii\bootstrap\Html::a('История переводов', ['transactions/credits-history'], ['class' =>'btn btn-primary']) ?></p>
            </div>
            <div class="col-lg-3">
                <h2>Счета</h2>
                <p><?= \yii\bootstrap\Html::a('Выставить счет', ['transactions/send-bill'], ['class' =>'btn btn-default']) ?></p>
                <p><?= \yii\bootstrap\Html::a('История счетов', ['transactions/bills-history'], ['class' =>'btn btn-primary']) ?></p>
            </div>
            <div class="col-lg-3">
                <h2>Баланс</h2>
                <p><?= \yii\bootstrap\Html::a('Проверить баланс', ['transactions/user-history'], ['class' =>'btn btn-success']) ?></p>
            </div>
        </div>
    </div>
</div>
