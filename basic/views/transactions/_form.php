<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model frontend\models\Office */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-md-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'credits')?>

    <?= $form->field($model, 'reciever_ip')?>

    <?php if (Yii::$app->controller->action->id == 'send-credits') {

        echo $form->field($model, 'protected')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]);
    }
        ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Create'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>