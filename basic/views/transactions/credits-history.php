<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Transactions;
use yii\grid\ActionColumn;

$transactions = new ActiveDataProvider([
    'query' => Transactions::find()->where(['sender_ip' => Yii::$app->user->identity->user_ip,'type' => 0])->orderBy(['id' => SORT_DESC]),
    'pagination' => [
        'pageSize' => 20,

    ],
]);

echo "<h4>ПЕРЕВОДЫ:</h4>";
echo GridView::widget([
    'dataProvider' => $transactions,
    'summary' => '',
    'columns' => [
        'sender_ip',
        'reciever_ip',
        'credits',
        'created_at:datetime',
        'status' => [
            'label' => 'Статус',
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'value' => function ($data) {
                if ($data->status == 1) {
                    return "Отклонен";
                } elseif ($data->status == 10) {
                    return "Ожидает подтверждения";
                } else {
                    return "Подтвержден";
                }

            },
        ],
    ]
]);