<?php
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Transactions;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo "<h3 class='text-center'>Ожидающие подтверждения:</h3>";
echo "<h4>СЧЕТА:</h4>";
echo GridView::widget([
    'dataProvider' => $pendingBills,
    'summary' => '',
    'columns' => [
        'sender_ip',
        'reciever_ip',
        'credits',
        'created_at:datetime',
        [
            'class' => ActionColumn::className(),
            'template' => '{accept} {decline}',
            'buttons' => [
                'accept' => function ($url, $model) {
                    return Html::a('Accept', ['transactions/accept-bill','id' => $model->id], ['class' => 'btn btn-success']);
                },

                'decline' => function ($url, $model) {
                    return Html::a('Decline', ['transactions/decline-bill','id' => $model->id], ['class' => 'btn btn-danger']);
                }

            ],
        ],
    ]
]);
echo "<h4>ПЕРЕВОДЫ:</h4>";
echo GridView::widget([
    'dataProvider' => $pendingTransactions,
    'summary' => '',
    'columns' => [
        'sender_ip',
        'reciever_ip',
        'credits',
        'created_at:datetime',
        [
            'class' => ActionColumn::className(),
            'template' => '{accept} {decline}',
            'buttons' => [
                'accept' => function ($url, $model) {
                    return Html::a('Accept',null, ['class' => 'btn btn-success', 'data-toggle' => 'modal','data-model-id' => $model->id, 'data-target' => '#myModal']);
                },

                'decline' => function ($url, $model) {
                    return Html::a('Decline', ['transactions/decline-transaction','id' => $model->id], ['class' => 'btn btn-danger']);
                }

            ],
        ],
    ]
]);


echo "<h3 class='text-center'>Подтвержденные:</h3>";
echo "<h4>ПЕРЕВОДЫ:</h4>";
echo GridView::widget([
    'dataProvider' => $doneTransactions,
    'summary' => '',
    'columns' => [
        'sender_ip',
        'reciever_ip',
        'credits',
        'created_at:datetime',
        'status' => [
            'label' => 'Статус',
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'value' => function ($data) {
                if ($data->status == 1) {
                    return "Отклонен";
                } else {
                    return "Подтвержден";
                }

            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{return}',
            'buttons' => [
                'return' => function ($url, $model) {
                    if ($model->status == 0) {
                        return Html::a('Вернуть', ['transactions/return-transaction','id' => $model->id], ['class' => 'btn btn-danger']);
                    }
                }

            ],
        ],
    ]
]);

echo "<h4>СЧЕТА:</h4>";
echo GridView::widget([
    'dataProvider' => $doneBills,
    'summary' => '',
    'columns' => [
        'sender_ip',
        'reciever_ip',
        'credits',
        'created_at:datetime',
        'status' => [
            'label' => 'Статус',
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'value' => function ($data) {
                if ($data->status == 1) {
                    return "Отклонен";
                } else {
                    return "Подтвержден";
                }

            },
        ],
        ]

]);

?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form action="<?= \yii\helpers\Url::toRoute(['transactions/accept-transaction']) ?>" enctype="multipart/form-data" method="post">
                    <?= Html::label('Enter Code here') ?>
                    <?= Html::input('string','protectionCode',null,['class' => 'form-control']) ?>
                    <?= Html::hiddenInput('modelId',null,['class' => 'form-control']) ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app','Submit'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
